package arreglos_actividad1;

import java.util.Scanner;

public class ARREGLOS_9_ACTIVIDAD1 {

    public static void main(String[] args) {
             Scanner entrada = new Scanner(System.in);
  
  String [] Dia = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
  int numero = 0;
  
        do {  
            System.out.println("Ingrese un numero del 1 al 12");
            numero = entrada.nextInt();
            
            if (numero > 0 && numero < 13 ) {
                
                if(numero <= 12)
                    System.out.println(Dia[numero -1]);
                
             else if ((numero % 12) == 0 ) 
                    System.out.println(Dia[6]);
             else 
                    System.out.println(Dia[(numero -1) % 12]);
            }else 
                System.out.println("Favor ingrese un numero en el rango 1 a 12");
            
        } while (numero < 1 && numero > 12);

    }
    
}
